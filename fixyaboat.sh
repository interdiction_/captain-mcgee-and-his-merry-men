#!/bin/bash
if [ "$*" == "" ]; then
    echo "bash fixyaboat.sh --yay"
    exit 1
fi
if [ "$1" == --yay ]
    then
    clear
    echo '''
Trawler mCgEe fixyaboat dependencies

    x   sSSs
     _, \_SSSS
      \\/((`\Ss
       \/)_|\\S
        /::|//S
        |:/ `SS
      __|/
     /:::\
     ``:/
    '''
    echo "Ahoey therrr, MaeTTy"
    echo "Letzz get the engine n sh!t installed"
    if [ $(which ruby 2>/dev/null | grep -c "ruby") -eq 0 ]
        then
        sudo apt install -y ruby
        else
        echo "ruby:         tick!"
    fi
    if [ $(gem list -i "spidr" 2>/dev/null | grep -c "true") -eq 0 ]
        then
        sudo gem install spidr
        else
        echo "spidr:        tick!"
    fi
    if [ $(which openssl 2>/dev/null | grep -c "openssl") -eq 0 ]
        then
        sudo apt install -y openssl
        else
        echo "openssl:      tick!"
    fi
    if [ $(which curl 2>/dev/null | grep -c "curl") -eq 0 ]
        then
        sudo apt install -y curl
        else
        echo "curl:         tick!"
    fi
    if [ $(which pip3 2>/dev/null | grep -c "pip3") -eq 0 ]
        then
        sudo apt install -y python3-pip
        else
        echo "pip3:         tick!"
    fi
    if [ $(which jq 2>/dev/null | grep -c "jq") -eq 0 ]
        then
        sudo apt install -y jq
        else
        echo "jq:           tick!"
    fi
    if [ $(which catimg 2>/dev/null | grep -c "catimg") -eq 0 ]
        then
        sudo apt install -y catimg
        else
        echo "catimg:           tick!"
    fi
    echo "letz start trawlin..........."
	exit 1
fi
