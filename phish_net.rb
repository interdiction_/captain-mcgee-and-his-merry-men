require 'spidr'
if ARGV[0] == "--net"
else
    puts "ruby phish_net.rb --net https://www.domain.com outputfile"
    exit
    end
if ARGV[1]
else
    puts "missing target"
    exit
    end
if ARGV[2]
else
    puts "missing outputfile"
    exit
    end
target = "#{ARGV[1]}"
outfile = "#{ARGV[2]}"
Spidr.site(target) do |spider|
    spider.every_url { |url| File.write(outfile, url, mode: "a") | File.write(outfile, "\n", mode: "a") }
    end
    `sort -u "#{ARGV[2]}" -o "#{ARGV[2]}"`
