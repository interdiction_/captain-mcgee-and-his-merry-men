#!/bin/bash
BAR='##############################'
FILL='------------------------------'
barLen=30
rm -R icebox/frozen/ 2>/dev/null
mkdir icebox/frozen
export var_runid=$(openssl rand -hex 12)
export var_10='sleep 10'
export var_4='sleep 0.7'
export var_curl10='curl -s -m 10 --connect-timeout 10'
export var_c10post='curl -s -m 10 --connect-timeout 10 --request POST --url'
export var_c10get='curl -s -m 10 --connect-timeout 10 --request GET --url'
export var_c10xpost='curl -m 10 --connect-timeout 10 -s -X POST'
export var_netconn='https://www.google.com/'
export var_ss1='sleep 0.1'
export var_sleepr='sleep 3'
export var_curl30='curl -s --connect-timeout 30'
export var_dater=$(date +%d-%m-%y)
export var_journ='date +%H:%M'
export var_apikey=$(cat captains.json |jq -r .apikey)
export var_slackr=$(cat captains.json |jq -r .slack)
export var_uuid='phish_to_sell.log'
export var_logg='captians.log'
export var_nme='trawler_mcgee.sh'
export var_run='ocean_map.csv'
export var_fish='marlins.csv'
export var_storage='icebox'
export var_stinker='phewwey.log'
rm $var_uuid $var_logg 2>/dev/null
#
trap func_trap INT
func_trap () {
    clear
	echo "captian was to drunk, boat crashed ah fuk me blimey"
	echo -n "$var_dater," >> "$var_run"
	echo "captain_exception,boat crashed ah fuk" >> "$var_run"
    rm $var_uuid $var_logg 2>/dev/null
	exit 1
}
#
func_storeit () {
    mv icebox/frozen/*.json coldstorage/
}
#
func_crewdrunk () {
	echo -n "$var_dater," >> "$var_run"
	echo "seaworthy,bon voyage,pass" >> "$var_run"
}
#
func_crewsober () {
	echo -n "$var_dater," >> "$var_run"
	echo "seaworthy,she has returned with a catch,pass" >> "$var_run"
}
#
func_todrunk () {
	echo -n "$var_dater," >> "$var_run"
	echo "crash,to drunk to catch phish,pass" >> "$var_run"
}
#
func_caughtphish () {
	echo -n "$var_dater," >> "$var_run"
	echo "fishmarket,catch sold successfully,pass" >> "$var_run"
}
#
func_nophish () {
	echo -n "$var_dater," >> "$var_run"
	echo "fishmarket,no phish to sell,pass" >> "$var_run"
}
#
func_paydayz () {
	echo -n "$var_dater," >> "$var_run"
	echo "fishmarket,collecin cashhh,pass" >> "$var_run"
}
#
func_bankroll () {
	echo -n "$var_dater," >> "$var_run"
	echo "fishmarket,marketz paid the crew,pass" >> "$var_run"
}
#
func_checkbad () {
    echo "X" >> $var_stinker
	echo -n "$var_dater," >> "$var_run"
	echo "checkdabook,$f,got a bad phish" >> "$var_run"
}
#
func_checkgood () {
	echo -n "$var_dater," >> "$var_run"
	echo "checkdabook,$f,not a bad fish" >> "$var_run"
}
#
func_hope () {
echo '''      _///_
     /o    \/
     > ))_./\hope we got fresh fish and not...
....._____.......
      /     \/|ded phish
      \x__  /\|
          \|'''
}
#
func_marlin () {
    while read fish
        do
        echo -n "$var_dater," >> "$var_fish"
        echo "marlin_catch,$fish" >> "$var_fish"
    done < $var_logg
}
#
func_inet_f () {
	n=0
	until [ "$n" -ge 15 ]
		do
		$var_curl30 $var_netconn >/dev/null && break
		n=$((n+1))
		sleep 5
		done
	if [ "$n" == 0 ]
		then
		$var_ss1
		elif [ "$n" -gt 0 ]
		then
		echo "rough seas [no inet conn]"
		echo -n "$var_dater," >> "$var_run"
		echo "rough-seas,couldnt connect to internet" >> "$var_run"
        rm $var_uuid $var_logg 2>/dev/null
		exit 1
	fi
}
#
fun_banner () {
clear
catimg -w 150 trawler.png
echo "TraWleR mCgEe and his merry crew!"
}
#
fun_memeface () {
clear
echo '''
      ////^\\\\
      | ^   ^ |
     @ (o) (o) @
      |   <   | Oi!! sailer!
      |  []_  | Your net doesnt contain vaild urls
       \_____/  Im taking you back to shore
     ____|  |____ 
    /    \__/    \
   /              \
  /\_/|        |\_/\
 / /  |        |  \ \
'''
}
#
if [ "$*" == "" ]; then
    fun_banner
    echo "[!]bash $var_nme --oicaptain"
    exit 1
fi
if [ "$1" == --oicaptain ]
    then
    fun_banner
    echo "[nodock]bash $var_nme domains.txt"
    echo "[docker]docker run -v ${PWD}:/captain-mcgee-and-his-merry-men -t --rm -i cmahmm domains.txt"
    echo ""
    echo "Ahoey therrr, MaeTTy! Are you ready?"
	exit 1
fi
if [ -z $(head -n 1 $1 |grep "[[:alnum:]]\+[[:alnum:]\-\.]\+[[:alnum:]]\+\.[[:alpha:]]\+\$") ]
    then
    fun_memeface
	echo -n "$var_dater," >> "$var_run"
	echo "captain_exception,sailor didnt pack the domains correctly" >> "$var_run"
    exit 1
fi
func_crewdrunk
func_inet_f
fun_banner
echo "Log entry - $($var_journ) captain is working hard, could be a while!"
while read line
    do
    ruby phish_net.rb --net https://$line $var_logg
done <"$1"
echo "Log entry - $($var_journ) lok at dat, $(wc -l $var_logg |cut -d ' ' -f1) potential pish"
totalLines=$(wc -l $var_logg | awk '{print $1}')
spidr_count=0
if [[ -s $var_logg ]]
    then
    echo "Log entry - $($var_journ) Real em IN"
    func_hope
    while read line
        do
        $var_c10xpost "https://urlscan.io/api/v1/scan/" \
        -H "Content-Type: application/json" \
        -H "API-Key: $var_apikey" \
        -d "{\"url\": \"$line\", \"public\": \"on\"}" |grep uuid |tr -d ' ' |sed 's/\"//g' |sed 's/,//g' |cut -f2 -d ":" >> $var_uuid
        count=$(($count + 1))
        percent=$((($count * 100 / $totalLines * 100) / 100))
        i=$(($percent * $barLen / 100))
        echo -ne "\r[${BAR:0:$i}${FILL:$i:barLen}] $count/$totalLines ($percent%)"
        $var_10
        done <$var_logg
    echo ""
    echo "Log entry - $($var_journ) caught some fish! sleepin...10"
    func_crewsober
    $var_10
    else
    echo "Log entry - $($var_journ) to drunk to find $var_logg"
    func_todrunk
    exit 1
fi
sort -u $var_uuid -o $var_uuid
count=0
if [[ -s $var_uuid ]]
    then
    echo "Log entry - $($var_journ) fish gonna sell!"
    func_caughtphish
    else
    echo "Log entry - $($var_journ) arh dam, couldnt sell da phish"
    func_nophish
    exit 1
fi
func_paydayz
fun_banner
echo "Log entry - $($var_journ) puttin phish on the market"
while read uuid_js
    do
    count=$(($count + 1))
    percent=$((($count * 100 / $totalLines * 100) / 100))
    i=$(($percent * $barLen / 100))
    echo -ne "\r[${BAR:0:$i}${FILL:$i:barLen}] $count/$totalLines ($percent%)"
    $var_4
    $var_curl10 "https://urlscan.io/api/v1/result/$uuid_js" > $var_storage/frozen/$uuid_js.json
done <$var_uuid
func_marlin
rm $var_uuid $var_logg 2>/dev/null
func_bankroll
echo ""
echo "Log entry - $($var_journ) Checkin books, for baD phIsh. Der might B somtin intersztin..."
echo ""
count=0
export totalcatch=$(ls icebox/frozen |wc -l |awk '{print $1}')
find coldstorage/. -name "*.json" -type 'f' -size -2k -delete
find icebox/frozen/. -name "*.json" -type 'f' -size -2k -delete
for f in $var_storage/frozen/*
    do
    count=$(($count + 1))
    percent=$((($count * 100 / $totalcatch * 100) / 100))
    i=$(($percent * $barLen / 100))
    echo -ne "\r[${BAR:0:$i}${FILL:$i:barLen}] $count/$totalcatch ($percent%)"
    if [ $(cat $f |jq '.verdicts[].malicious' |grep false |uniq) ]
        then
        func_checkgood
    fi
    if [ $(cat $f |jq '.verdicts[].malicious' |grep true |uniq) ]
        then
        func_checkbad
    fi
done
echo ""
if [ -f $var_stinker ]
    then
    ohshit=$(wc -l $var_stinker | cut -f1 -d " ")
    export var_bot=$(cat captians.json |jq -r .slackbot)
    export var_chn=$(cat captians.json |jq -r .slackchannel)
    curl -s -m 10 --retry 10 --connect-timeout 10 -X POST --data-urlencode "payload={\"channel\": \"$var_chn\", \"username\": \"$var_bot\", \"text\": \"ID: Trawler Mcgee - malware detected\nStinkers: $ohshit new phish\"}" "$var_slackr" > /dev/null
    echo "stinkin phish found! DO SOMETHING ABOUT IT!"
    rm $var_stinker
    else
    echo "You got no stinky phish, your lucky!"
fi
func_storeit
echo "until next time sonny jim..."