<div align="center">

<img src="images/CgY3NxxSPae7541629-cb0b-4dc2-bd03-7c1711314425-1632433471.png" width="80">

# Captain Mcgee and his Merry Men

:rowboat: Another OSINT tool.

![codebase](https://img.shields.io/badge/codebase-ruby-red) ![codebase](https://img.shields.io/badge/codebase-bash-black)
![leverages](https://img.shields.io/badge/leverages-urlscan-green)
![workswith](https://img.shields.io/badge/workswith-splunk-green) ![workswith](https://img.shields.io/badge/workswith-elk-blue) ![workswith](https://img.shields.io/badge/workswith-kali-red)

Website scraper that submits URLs to urlscan and imports into Splunk!

Do you want to have a laugh and check your OSINT targets for potential malicious artefact's?

**Become a merry man/person/computer/something today!**

<img src="images/lel.gif" width="350">
</div>

<div align="center">

## <img src="images/crab.png" width="28">  Are you ready to be surrounded by seaman? 

</div>

```
git clone https://gitlab.com/interdiction_/captain-mcgee-and-his-merry-men
```
<div align="center">

###### dependencies [nodocker]

</div>

```
cd captain-mcgee-and-his-merry-men/
bash fixyaboat.sh --yay
```

```
Trawler mCgEe fixyaboat dependencies 

    x   sSSs
     _, \_SSSS
      \\/((`\Ss
       \/)_|\\S
        /::|//S
        |:/ `SS
      __|/
     /:::\
     ``:/

Ahoey therrr, MaeTTy
Letzz get the engine n sh!t installed
ruby:         tick!
spidr:        tick!
openssl:      tick!
curl:         tick!
pip3:         tick!
jq:           tick!
catimg:       tick!
letz start trawlin...........
```

<div align="center">

## <img src="images/srimp.png" width="28"> Things you should know before boarding

</div>

**Steps to become ship shape**

- [x] :computer: **if your not using docker** `bash fixyaboat.sh --yay` to ensure dependencies are installed
- [x] :card_index: data is output to `icebox/frozen` while running and then stored in `coldstorage`
- [x] :tropical_fish: `marlins.csv` is your scraped urls. Example `25-09-21,blahblahrandom.json,phish is good|bad`
- [x] :fish: `oceanmap.csv` is your run log. Example `25-09-21,seaworthy,bon voyage`
- [x] :floppy_disk: `captains.json` is your json config file for your API keys and notifcations
- [x] :whale2: Use the docker image if you want [recommended]
- [x] :tada: Enjoy the dank memes

<div align="center">

## :whale2: No whaling [no docker]

</div>

```
cd captain-mcgee-and-his-merry-men/
bash trawer_mcgee.sh --oicaptain
```

<div align="center">

<img src="images/label.png">

</div>

<div align="center">

## :whale: whaling [docker]

_no whales were hurt coding this_

</div>

```
FROM kalilinux/kali-rolling
LABEL CMAHMM "inteloperator"
RUN apt update
RUN apt install -y ruby python3-pip openssl curl
RUN gem install spidr
RUN apt install -y jq
WORKDIR "/captain-mcgee-and-his-merry-men"
ENTRYPOINT ["bash", "trawler_mcgee.sh"]
```

```
cd captian-mcgee-and-his-merry-men/
docker build -t cmahmm . --no-cache
docker run -v ${PWD}:/captain-mcgee-and-his-merry-men -t --rm -i cmahmm --oicaptain
docker run -v ${PWD}:/captain-mcgee-and-his-merry-men -t --rm -i cmahmm domains.txt
```

<div align="center">

## :fishing_pole_and_fish: Get the trawler shipshape!

</div>

**EDIT `captains.json` and add your urlscan api key.**

```
nano captain-mcgee-and-his-merry-men/captains.json
{
    "apikey": "",
    "slack": "",
    "slackchannel": "",
    "slackkbot": ""
}
```

<div align="center">

## :notebook: Operating the trawler, when the captain is drunk

</div>

<div align="center">
<img src="images/drunk.gif" width="350">

##### no dock[ing|er]

</div>

```
cd captain-mcgee-and-his-merry-men/
bash trawler_mcgee.sh listofdomainstocrawlenotdrunk.txt
```
<div align="center">

**if your in the mood for docking ha! meme, no serious**

</div>

```
cd captain-mcgee-and-his-merry-men/
docker run -v ${PWD}:/captain-mcgee-and-his-merry-men -t --rm -i cmahmm domains.txt
```

<div align="center">

### <img src="images/hlp.png" width="28"> Boat upgrades

</div>

**Trawler upgrade current state:**

- [x] TelliN the lassys about your adventures [Slack notifcations]
- [x] Dank memes [just cause]
- [x] Can i sell to phish local zealand market [json phish exports]
- [x] Rough seas check [internet connection checker]
- [x] Trawlers fishing net made [ruby crawler]

<div align="center">

## :ledger: Looking at the captains log

`cat captain-mcgee-and-his-merry-men/ocean_map.csv`

<img src="images/what.png" width="450">

## :books: Looking at the phish log

`cat captain-mcgee-and-his-merry-men/marlins.csv`

<img src="images/marlin.png" width="600">

## :dolphin: Examples of past trips

**Check the icebox!**

</div>

```
anon@phishingmemekali:/dontrevealdir/captain-mcgee-and-his-merry-men$ ls -l icebox/
-rwxrwxrwx 1 anon anon  12277 [redacted] 01a9b5db-61ab-4d4f-9736-2bc7d67c0ff5.json
-rwxrwxrwx 1 anon anon  12722 [redacted] 45dc5492-7705-4d61-86b5-a81a247ff262.json
-rwxrwxrwx 1 anon anon  12302 [redacted] 52d85120-9fb9-4abb-94c8-419d79471d4f.json
-rwxrwxrwx 1 anon anon  12297 [redacted] 67ac5bd9-4005-431b-b26f-b31c702b5229.json
-rwxrwxrwx 1 anon anon 416988 [redacted] 829f12ca-bb9f-48fa-a58a-e2ecc7e2771b.json
-rwxrwxrwx 1 anon anon  12348 [redacted] 8f5d0a4a-cc4a-4592-bd01-dcea24b89587.json
-rwxrwxrwx 1 anon anon  12729 [redacted] a8609e6d-02d3-4d5d-9af5-c36e7bf88888.json
-rwxrwxrwx 1 anon anon  12658 [redacted] af0ee5d4-ee85-4423-b5d0-c11ab933860c.json
-rwxrwxrwx 1 anon anon  12344 [redacted] afb12ee1-30f6-4617-bc28-3dd2d06adf19.json
```
