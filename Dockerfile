#docker build -t cmahmm .
#docker run -v ${PWD}:/captain-mcgee-and-his-merry-men -t --rm -i cmahmm domains.txt
FROM kalilinux/kali-rolling 
LABEL CMAHMM "inteloperator"
RUN apt update
RUN apt install -y ruby python3-pip openssl curl catimg
RUN gem install spidr
RUN apt install -y jq
WORKDIR "/captain-mcgee-and-his-merry-men"
ENTRYPOINT ["bash", "trawler_mcgee.sh"]